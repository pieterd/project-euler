﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Euler.Problems
{
    class Problem2
    {
        internal static void Solution()
        {
            Solution1();
            Solution2();
        }
        // By considering the terms in the Fibonacci sequence whose values do not exceed four million, find the sum of the even-valued terms.
        internal static void Solution1()
        {
            int previousTerm = 0;
            int term = 1;
            int totalOfEvenTerms = 0;
            for (int i = 1; i < 4000000; i = previousTerm + term)
            {
                previousTerm = term;
                term = i;

                if (term % 2 == 0)
                {
                    totalOfEvenTerms += term;
                }
            }
            Console.WriteLine(totalOfEvenTerms); 
        }
        internal static void Solution2()
        {
            int previousTerm = 0;
            int term = 1;
            int totalOfEvenTerms = 0;
            for (int i = 1; i < 4000000; i = previousTerm + term)
            {
                previousTerm = term;
                term = i;
                if ((term & 1) == 0)  
                {
                    totalOfEvenTerms += term;
                }
            }
            Console.WriteLine(totalOfEvenTerms);
        }
    }
}
