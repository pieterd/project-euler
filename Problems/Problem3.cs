﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Euler.Problems
{
    class Problem3
    {
        // What is the largest prime factor of the number 600851475143 ?
        internal static void Solution()
        {
            //Solution1();
            Solution2();
        }

        private static void Solution1()
        {
            double product = 600851475143;

            // The square root of the product is the theoretically biggest factor of the product. No need to look for primes beyond the square root
            int sqrtOfProduct = (int)Math.Sqrt(product);

            IEnumerable<int> allInts = Enumerable.Range(2, sqrtOfProduct);
            IEnumerable<int> allPrimes = allInts.Where(x => !allInts.TakeWhile(y => y < x).Any(y => x % y == 0));

            List<double> allFactors = new List<double>();

            allFactors.Add(GetNextFactor(product, allPrimes, 1));

            while (allFactors.Aggregate((total, nextFactor) => total * nextFactor) < product)
            {
                allFactors.Add(GetNextFactor(product, allPrimes, allFactors.Last()));
            }

            Console.WriteLine("The highest factor = " + allFactors.OrderByDescending(x => x).First());
        }

        private static int GetNextFactor(double product, IEnumerable<int> allPrimes, double lastFoundFactor)
        {
            return allPrimes.Where(x => x > lastFoundFactor && product % x == 0).First();
        }

        private static void Solution2()
        {
            Int64 product = 600851475143;
            Int64 i = 2;
            while (product > 1)
            {
                // product can be divisible by the prime found more than once
                while (product % i == 0)
                {
                    Console.WriteLine("found prime " + i);
                    product /= i;
                }
                i++;
            }
        }

    }
}
