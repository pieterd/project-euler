﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Project_Euler.Problems
{
    static class Problem4
    {
        // A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
        // Find the largest palindrome made from the product of two 3-digit numbers.
        internal static void Solution()
        {
            Solution1();
        }


        private static void Solution1()
        {
            //530ms
            Int64 largest = 0;
            int ii=0, jj=0;
            foreach (var i in Enumerable.Range(100,899))
            {
                foreach (var j in Enumerable.Range(100,899))
                {
                    if ((i * j).ReverseDigits() == (i * j).ToString())
                    {
                        if ((i * j) > largest)
                        {
                            largest = i * j;
                            ii = i;
                            jj = j;
                        }
                    }
                }   
            }
            //for (var i = 100; i <= 999; i++)
            //{
            //    for (var j = 100; j <= 999; j++)
            //    {
            //        if ((i*j).ReverseDigits() == (i*j).ToString())
            //        {
            //            if ((i*j) > largest)
            //            {
            //                largest = i*j;
            //            }
            //        }
            //    }
            //}
            Console.WriteLine("largest: " + largest + "|" + ii + "|" + jj);
        }

        private static string ReverseDigits(this int number)
        {
            return new String(number.ToString().ToCharArray().Reverse().ToArray());
        }
    }
}
