﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Euler.Problems
{
    class Problem1
    {
        // Find the sum of all the multiples of 3 or 5 below 1000.
        internal static void Solution()
        {
            Solution1();
            Solution2();
        }

        internal static void Solution1()
        {
            List<int> multiplesOfThree = new List<int>();
            List<int> multiplesOfFive = new List<int>();

            for (int i = 0; i < 1000; i++)
            {
                if (i % 3 == 0)
                {
                    multiplesOfThree.Add(i);
                    //Console.WriteLine(i);
                }
                else if (i % 5 == 0)
                {
                    multiplesOfFive.Add(i);
                    //Console.WriteLine(i);
                }
            }

            List<int> multiplesOfFiveAndThree = new List<int>();
            multiplesOfFiveAndThree.AddRange(multiplesOfThree);
            multiplesOfFiveAndThree.AddRange(multiplesOfFive);
            Console.WriteLine("poging 1: " + multiplesOfFiveAndThree.Sum());
        }
        internal static void Solution2()
        {
            Console.WriteLine("poging 2: " + Enumerable.Range(1, 1000 - 1).Where(i => i % 3 == 0 || i % 5 == 0).Sum());
        }
    }
}
