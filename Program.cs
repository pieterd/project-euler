﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project_Euler.Problems;

namespace Project_Euler
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            Problem4.Solution();

            stopwatch.Stop();
            Console.WriteLine("executed in " + stopwatch.ElapsedMilliseconds + "ms");
            Console.ReadKey();
        }


    }
}
